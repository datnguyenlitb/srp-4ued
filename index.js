const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const Auth = require('./controllers/auth.controller');
app.post('/auth/login', Auth.doLogin);

const Score = require('./controllers/score.controller');
app.post('/score/index', Score.doGetAllScores);

app.listen(port, () => console.log(`Listening on port ${port}`));
