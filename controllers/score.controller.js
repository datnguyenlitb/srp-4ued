const puppeteer = require('puppeteer');
const fs = require('fs').promises;

class ScoreController {
  constructor() {}

  async doGetAllScores(req, res, next) {
    try {
      const browser = await puppeteer.launch({ headless: true });
      const page = await browser.newPage();

      const cookiesString = await fs.readFile('./cookies.json');
      const cookies = JSON.parse(cookiesString);
      await page.setCookie(...cookies);

      await page.setViewport({ width: 1280, height: 720 });
      await page.goto('http://qlht.ued.udn.vn/sinhvien/diem/xemketquahoctap');

      await page.waitFor(2000);

      let data = [];
      data = await page.evaluate(() => {
        let fieldsets = Array.from(
          document.querySelectorAll('#frmMain fieldset')
        );

        let tables = fieldsets.map(fieldset => {
          let name = fieldset.querySelector('legend h2').innerText;
          let subjects = [];
          let htmlSubjects = fieldset.querySelectorAll('div > table > tbody > tr');
          
          htmlSubjects.forEach(item => {
            let subject = {};

            console.log(item);

            try {
              subject.name = item.querySelector('td:nth-child(3)').innerText;
              subject.average = item.querySelector(
                'td:nth-child(6)'
              ).innerText;
              subject.type = item.querySelector('td:nth-child(8)').innerText;
            } catch (exception) {}

            subjects.push(subject);
          });

          return {
            name,
            resultOfTerm
          };
        });

        return tables;
      });

      res.send(data);
    } catch (err) {
      console.log(err);
      return res.status(400).send({
        message: 'This is an error!',
        err: err
      });
    }
  }
}

module.exports = new ScoreController();
