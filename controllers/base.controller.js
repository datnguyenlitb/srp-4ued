const autoBind = require('auto-bind');
class BaseController {
    constructor() {
        autoBind(this);
        this._model = 'baseModel'; 
    }

    index(req, res, next) {
        res.send('...')
    }

    create(req, res, next) {
        res.send(this._model)
    }

    update(req, res, next) {
        res.send('update func')
    }

    delete(req, res, next) {
        res.send('delete func')
    }
}

module.exports = BaseController;                                      
