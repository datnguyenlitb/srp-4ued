const puppeteer = require('puppeteer');
const fs = require('fs').promises;

class AuthControler {
  constructor() {}

  async doLogin(req, res, next) {
    try {
      // const {
      //   scode,
      //   password
      // } = req.body;

      const browser = await puppeteer.launch({ headless: true });
      const page = await browser.newPage();

      await page.goto('http://qlht.ued.udn.vn/');
      const username = await page.$(
        'input[type=text][name=txt_Login_ten_dang_nhap]'
      );
      await username.type('312023161109');
      const password = await page.$(
        'input[type=password][name=pw_Login_mat_khau]'
      );
      await password.type('datpro');
      // await password.type('01687890890');
      await page.click('input[type=submit][name=bt_Login_submit]');

      await page.waitForNavigation();
      const cookies = await page.cookies();
      await fs.writeFile('./cookies.json', JSON.stringify(cookies, null, 2));

      res.json({
        r: 1,
        msg: 'login_successful'
      });
    } catch (err) {
      return res.status(400).send({
        message: 'This is an error!'
      });
    }
  }
}
module.exports = new AuthControler();
